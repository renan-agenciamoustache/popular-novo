<?php
/**
 * @version    1.0
 * @package    urna
 * @author     Thembay Team <support@thembay.com>
 * @copyright  Copyright (C) 2019 Thembay.com. All Rights Reserved.
 * @license    GNU/GPL v2 or later http://www.gnu.org/licenses/gpl-2.0.html
 *
 * Websites: https://thembay.com
 */

add_action('wp_enqueue_scripts', 'urna_child_enqueue_styles', 10000);
function urna_child_enqueue_styles() {
	$parent_style = 'urna-style';
	wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'urna-child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
}

// Altera a ordem dos links na página "Minha Conta"

function my_account_menu_order() {
	$menuOrder = array(
		'dashboard'          => __( 'Painel de controle', 'woocommerce' ),
		'orders'             => __( 'Meus Pedidos', 'woocommerce' ),
		'edit-address'       => __( 'Endereco', 'woocommerce' ),
		'edit-account'    	=> __( 'Detalhes da Conta', 'woocommerce' ),
		'payment-methods'   => __( 'Métodos de Pagamento', 'woocommerce'),
		'customer-logout'    => __( 'Logout', 'woocommerce' ),
	);
	return $menuOrder;
}
add_filter ( 'woocommerce_account_menu_items', 'my_account_menu_order' );