<?php
/**
 * Customer new account email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-new-account.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;

do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s Customer username */ ?>
<div class="email-top email-top--newaccount">
	<p class="email-top--title" >Olá, <strong><?php echo $user_login; ?></strong></p>
	<p class="email-top--subtitle">Bem vindo ao Liquida Popular</p>
</div>
<div class="email-content">
	<p class="email--newaccount-text">Para acessar sua conta, basta inserir os dados abaixo na área de login em nosso site: </p>
	<p class="email--newaccount-user"><strong><?php echo 'Usuario: ' ?></strong><?php echo $email->user_email; ?></p>
	<p class="email--newaccount-pass"><strong><?php echo 'Senha: ' ?></strong><?php echo $email->user_pass; ?></p>
</div>	

<a href="http://localhost/popular1/produtos-em-promocao/" target="_blank" title="Produtos em Promoção" class="btn-email-ofertas">Ver Ofertas</a>

<?php

do_action( 'woocommerce_email_footer', $email );
