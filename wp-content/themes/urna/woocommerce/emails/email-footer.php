<?php
/**
 * Email Footer
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-footer.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
															</div>
														</td>
													</tr>
												</table>
												<!-- End Content -->
											</td>
										</tr>
									</table>
									<!-- End Body -->
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td align="center" valign="top">
						<!-- Footer -->
						<table border="0" cellpadding="10" cellspacing="0" width="600" id="template_footer">
							<tr>
								<td valign="top">
									<table border="0" cellpadding="10" cellspacing="0" width="100%">
										<tr>
											<td colspan="2" valign="middle" id="credit">
												<span class="footer--info"> *Mensagem Automática, favor não responder* </span>
												<div class="footer-links">
													<a href="#" target="_blank" class="footer-links-item"><img src="http://localhost/popular1/wp-content/uploads/2020/01/icon-facebook.jpg" alt="Ícone Facebook"></a>
													<a href="#" target="_blank" class="footer-links-item"><img src="http://localhost/popular1/wp-content/uploads/2020/01/icon-instagram.jpg" alt="Ícone Instagram"></a>
													<a href="#" target="_blank" class="footer-links-item"><img src="http://localhost/popular1/wp-content/uploads/2020/01/icon-url.jpg" alt="Ícone Site"></a>
												</div>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						<!-- End Footer -->
					</td>
				</tr>
			</table>
		</div>
	</body>
</html>
