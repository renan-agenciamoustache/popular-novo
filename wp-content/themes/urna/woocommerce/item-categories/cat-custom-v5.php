<?php 

$cat                =   get_term_by( 'id', $tab['category'], 'product_cat' );
$description        =   ( isset($tab['description']) ) ? $tab['description'] : '';
 

if( isset($cat) && $cat ) {
    $cat_name       =   $cat->name;    
    $cat_slug       =   $cat->slug;   
    $cat_link       =   get_term_link($cat->slug, 'product_cat');
    $cat_count      =   $cat->count;
} else {
    $cat_name       = esc_html__('Shop', 'urna');
    $cat_link       =   get_permalink( wc_get_page_id( 'shop' ) );
    $cat_count      =   urna_total_product_count();
}

if( isset($tab['check_custom_link']) &&  $tab['check_custom_link'] == 'yes' && isset($tab['custom_link']) && !empty($tab['custom_link']) ) {
    $cat_link = $tab['custom_link'];
}

?>
<div class="item-cat item-cat-v5 tbay-image-loaded">

    <div class="content-img">

        <?php if ( isset($tab['images']) && !empty($tab['images']) ): ?>
            <?php
                $cat_id         =   $tab['images'];    
                $image         = wp_get_attachment_url( $cat_id );
            ?>
            <div class="cat-img">
                <a href="<?php echo esc_url($cat_link); ?>">
                    <?php urna_tbay_src_image_loaded($image, array('alt'=> $cat_name )); ?>
                </a>
            </div>
        <?php endif; ?>

        <div class="content">
            <div class="text-cat-name">
                <?php if( isset($cat) && $cat ) : ?>
                    <a href="<?php echo esc_url($cat_link); ?>" class="cat-name"><?php echo trim($cat_name); ?></a>
                <?php else: ?>
                    <a href="<?php echo esc_url($cat_link); ?>" class="cat-name"><?php esc_html_e( 'All', 'urna' ) ?></a>
                <?php endif; ?>

            </div>

            <div class="img-desc hidden-sm hidden-xs hidden-md">
                <?php if ( isset($tab['sub_images']) && !empty($tab['sub_images']) ): ?>
                    <?php
                        $cat_id         =   $tab['sub_images'];    
                        $image         = wp_get_attachment_url( $cat_id );
                    ?>
                    <div class="cat-sub-img">
                        <?php urna_tbay_src_image_loaded($image, array('alt'=> $cat_name )); ?>
                    </div>
                <?php endif; ?>
                
                <?php if ( (isset($shop_now) && $shop_now == 'yes') ) { ?>
                    <div class="cat-hover">
                        <?php if ( $count_item == 'yes' ) { ?>
                            <span class="count-item"><?php echo trim($cat_count).' '. apply_filters( 'urna_tbay_categories_count_item', esc_html__('items','urna') ); ?></span>
                        <?php } ?>
                        <a href="<?php echo esc_url($cat_link); ?>" class="shop-now"><?php echo trim($shop_now_text); ?></a>
                    </div>
                    <?php }
                    else { ?>
                    <?php if ( $count_item == 'yes' ) { ?>
                        <span class="count-item"><?php echo trim($cat_count).' '.apply_filters( 'urna_tbay_categories_count_item', esc_html__('items','urna') ); ?></span>
                    <?php } ?>      
                <?php } ?>

                <?php if( !empty($description) ) : ?>
                    <div class="cat-description">
                        <?php echo trim($description); ?>
                    </div>
                <?php endif; ?>
            </div>

            <?php if ( (isset($shop_now) && $shop_now == 'yes') ) : ?>
                <a href="<?php echo esc_url($cat_link); ?>" class="shop-now"><?php echo trim($shop_now_text); ?></a>
            <?php endif; ?>
        </div>
    </div>

</div>