<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Urna
 * @since Urna 1.0
 */
/*

*Template Name: 404 Page
*/
get_header();
$custom_link	= urna_tbay_get_config('contact_select'); 

$url_link = get_the_permalink($custom_link);
?>

<section id="main-container" class=" container inner bg-error-404">
	<div id="main-content" class="main-page page-404">

		<section class="error-404 text-center">
			<h1><?php esc_html_e( 'Página não encontrada', 'urna' ); ?></h1>
			<div class="page-content">
				<p class="sub-title"><?php esc_html_e( 'Lamentamos, mas a página que você está procurando não existe ou foi movida. Por favor, volte à página inicial ou entre em contato conosco se for um erro.', 'urna' ); ?></p>
				<div class="group">
					<a class="backtohome" href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('voltar para home', 'urna'); ?></a>
					<a class="contactus" href="<?php echo esc_url($url_link); ?>"><?php esc_html_e('entrar em contato', 'urna'); ?></a>
				</div>
			</div><!-- .page-content -->
		</section><!-- .error-404 -->
	</div>
</section>

<?php get_footer(); ?>