=== Urna Core ===

Contributors: automattic
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments
Requires at least: 4.0
Tested up to: 5.0.0
Stable tag: 4.0.3
License: GPLv2 or later

A starter theme called Urna Core, or underscores.

== Description ==

Urna Core is a Organic & Food WooCommerce WordPress Theme that is flexible and customizable for setting and changing any elements wihtin a minutes via Powerful Theme Options, you also can customize Google fonts without code very easy and simple. 


== Installation ==

1. Upload the entire urna-core folder to the /wp-content/plugins/ directory.
2. Activate the plugin through the ‘Plugins’ menu in WordPress.

== Changelog ==

= 1.1.3 =
*Release Date - August 28 2019*
- Fix bug don't translate text on Instagram addons

= 1.1.2 =
*Release Date - August 26 2019*
- Add new button "Refresh All Data (Add the new Demos)" in Demo Importer

= 1.1.1 =
*Release Date - August 24 2019*
- Fix bug Instagram has returned invalid data

= 1.1 =
*Release Date - August 23 2019*
- Fix bug Instagram has returned invalid data

= 1.0.0 =
*Release Date - April 30 2019*
- Initial release