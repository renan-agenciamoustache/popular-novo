��          �       |      |     }     �     �     �     �     �  	   �  	   �  &   �          )     6     >     N  &   `     �     �     �     �     �  "   �  �  �     �     �     �     �          0     G     X  .   _     �     �     �     �  *   �  /   �     .     N  $   W  
   |     �  !   �    (maxsize:   Newest first  Oldest first  Upload max filesize.  kB, max files: 2 %s star %s stars Add Image All stars Based on %s review Based on %s reviews Choose pictures File upload: Filters Max file upload Max file uploads: Please agree with our term and policy. Please enter category title Stars Upload max filesize: Verified With images {customer_name} - Customer's name. Project-Id-Version: Photo Reviews for WooCommerce
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-07-25 10:37+0000
PO-Revision-Date: 2020-01-21 17:50+0000
Last-Translator: Moustache <renan@agenciamoustache.com.br>
Language-Team: Português do Brasil
Language: pt_BR
Plural-Forms: nplurals=2; plural=n != 1;
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3.2 (tamanho máximo: Recentes primeiro Antigos primeiro Tamanho máximo do upload kb, máximo 2 arquivos %s estrela %s estrelas Adicionar Imagem Todas  Baseado em %s análise Baseado em %s análises Escolha as imagens Upload de arquivo: Filtros Tamanho máximo upload Quantidade máxima de arquivos para upload Por favor aceite com nossos termos e política. Por favor escolha uma categoria Estrelas Tamanho máximo do arquivo de upload Verificado Com imagens {customer_name} - Nome do Cliente 