��    '      T      �      �  	   �  &   �     �     �     �     �     �     �               !     3     @     G     S     `     h     n     w     �     �     �     �  
   �     �     �  
   �     �  	   �     �                    '     ?     H     M     V  �  r     O     X     w     �     �     �     �     �     �     �     �                    /     >     L     T     ]     o     �     �  
   �     �     �     �     �     �     �     �  	   	  	   	     	     4	  	   P	     Z	  	   c	     m	    products %s customer review %s customer reviews &larr; Previous Account Add to wishlist All All Categories All categories Best Seller Best Selling Continue Shopping Edit Account Filter My Wishlist New Arrivals On Sale Order Previous Previous Post Previous page Price Product Quantity Quick View Search Form Search for products... Search in: Shopping cart Top Rated Update Cart View All View all View wishlist View your shopping cart Wishlist sold view all was added to shopping cart. Project-Id-Version: Thembay Theme
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=2; plural=n != 1;
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2020-01-13 15:07+0000
PO-Revision-Date: 2020-01-13 15:35+0000
Last-Translator: Moustache <renan@agenciamoustache.com.br>
Language-Team: Português do Brasil
Language: pt_BR
X-Generator: Loco https://localise.biz/
X-Loco-Version: 2.3.1; wp-5.3.1 produtos %s avaliação %s avaliações &larr; Anterior Conta Adicionar à lista de desejos Todos Todas Categorias Todas categorias Mais Vendidos Mais Vendidos Continuar Comprando Editar conta Filtrar Minha Lista de desejos Novos Produtos Na Promoção Pedidos Anterior Postagem Anterior Página anterior Preço Produto Quantidade Espiada Formulário de busca Faça uma busca... Procurar em: Carrinho de compras Mais Votados Atualizar Carrinho Ver Todos Ver todos Ver lista de desejos Ver seu carrinho de compras Favoritos vendidos ver todos foi adicionado ao carrinho. 