<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */



// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'popular' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', $_ENV["DATABASE_USERNAME"] ? $_ENV["DATABASE_USERNAME"] : 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', $_ENV["DATABASE_USERNAME"] ? $_ENV["DATABASE_PASSWORD"] : '@Moust753ache*' );

/** Nome do host do MySQL */
define( 'DB_HOST', $_ENV["DATABASE_HOST"] ? $_ENV["DATABASE_HOST"] :'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'k%&UfP:-+9N7GytZ52[s[Dp[?Jt8FhisY&&4HoDuXdPy1w6golFZyCo][z&O<Q,r' );
define( 'SECURE_AUTH_KEY',  'Ez~[_)pgL4AWiyZNS+[CV+F=+WIp 9zRHgyaQa^kRs}b^-BslaRY3%%vVP2D HYR' );
define( 'LOGGED_IN_KEY',    '%az<b{q!WH{g8j<F?le<T_j^`5Q5l=`GY83&*?ykFz*z|WDSJdEV- Hk$0@`FR7D' );
define( 'NONCE_KEY',        'ez&Cx;qjivE@0:846~=2[Q,?^AhEuLwi<AM]}a!W@>sOFWu5ycYs9~};Tl_)ZoaW' );
define( 'AUTH_SALT',        'W#3@S9[xfo)(D^VP3Qr|jY&xE{>F;}peNK}TY6=t+QO}=6uNcgt;k(jBG*+whl~1' );
define( 'SECURE_AUTH_SALT', '.$;Q}>H>plozQ1A}EXAo84u|FX64^ntPErW[Kt9i>3dtV=SujNu{&TlFcYB$E6R+' );
define( 'LOGGED_IN_SALT',   '38_pI=}ZvT~f9OSASdow=6`8?aj-)@RZk/=fV(|1Ll|dQ;&z+>~irbv+Vil_)No.' );
define( 'NONCE_SALT',       'A% A]EjmWaIJx8</R0wG^t+hX4HGnL{+KZ:k5PZ4)RE-G1ih(Q9~q]`P$FZ2lV:A' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_popular_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');

define ('WP_MEMORY_LIMIT', '256M');

